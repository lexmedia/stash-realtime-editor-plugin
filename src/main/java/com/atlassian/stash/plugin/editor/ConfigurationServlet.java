package com.atlassian.stash.plugin.editor;

import com.atlassian.fugue.Either;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.user.Permission;
import com.atlassian.stash.user.PermissionService;
import com.atlassian.stash.user.StashAuthenticationContext;
import com.atlassian.stash.user.StashUser;
import com.google.common.collect.ImmutableMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.Map;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;
import static org.apache.commons.lang.StringUtils.isBlank;

public class ConfigurationServlet extends HttpServlet {

    private final StashAuthenticationContext authenticationContext;
    private final CollaborationService collaborationService;
    private final LoginUriProvider loginUriProvider;
    private final ServletSupport servletSupport;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final PermissionService permissionService;
    private final WebResourceManager webResourceManager;

    public ConfigurationServlet(StashAuthenticationContext authenticationContext, CollaborationService collaborationService,
                                LoginUriProvider loginUriProvider, ServletSupport servletSupport,
                                SoyTemplateRenderer soyTemplateRenderer, PermissionService permissionService,
                                WebResourceManager webResourceManager) {
        this.authenticationContext = authenticationContext;
        this.collaborationService = collaborationService;
        this.loginUriProvider = loginUriProvider;
        this.servletSupport = servletSupport;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.permissionService = permissionService;
        this.webResourceManager = webResourceManager;
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setEnabledForRequest(req, resp, false);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Repository repo = tryGetRepo(req, resp);
        if (repo == null) {
            return;
        }
        if (tryGetAuthorizedUser(req, resp, repo) == null) {
            return;
        }
        render(resp, "stash.editor.config", ImmutableMap.<String, Object>builder()
                .put("repository", repo)
                .put("isEnabled", collaborationService.isEnabledForRepository(repo))
                .build());
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setEnabledForRequest(req, resp, true);
    }

    private void render(HttpServletResponse resp, String templateName, Map<String, Object> data) throws IOException, ServletException {
        resp.setContentType("text/html;charset=UTF-8");
        try {
            webResourceManager.requireResourcesForContext("plugin.page.editor.config");
            soyTemplateRenderer.render(resp.getWriter(),
                    "com.atlassian.stash.plugin.stash-editor-plugin:editor-server-side-soy", templateName, data);
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            throw new ServletException(e);
        }
    }

    private void setEnabledForRequest(HttpServletRequest req, HttpServletResponse resp, boolean enabled) throws IOException, ServletException {
        Repository repo = tryGetRepo(req, resp);
        if (repo == null) {
            return;
        }
        if (tryGetAuthorizedUser(req, resp, repo) == null) {
            return;
        }

        collaborationService.setEnabledForRepository(repo, enabled);
        resp.setStatus(204);
    }

    private StashUser tryGetAuthorizedUser(HttpServletRequest req, HttpServletResponse resp, Repository repo) throws ServletException, IOException {
        StashUser user = authenticationContext.getCurrentUser();
        if (user == null || !permissionService.hasRepositoryPermission(user, repo, Permission.REPO_ADMIN)) {
            if (user == null) {
                // redirect to login
                StringBuffer originalUrl = req.getRequestURL();
                if (!isBlank(req.getQueryString())) {
                    originalUrl.append("?").append(req.getQueryString());
                }
                resp.sendRedirect(loginUriProvider.getLoginUri(URI.create(originalUrl.toString())).toASCIIString());
                return null;
            }
            resp.sendError(SC_UNAUTHORIZED, "You don't have permission to access this.");
            return  null;
        }
        return user;
    }

    private Repository tryGetRepo(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Either<String, EntityDescriptor> entityDescriptorOrError = servletSupport.resolveEntity(req, resp);

        if (entityDescriptorOrError.isLeft()) {
            String error = entityDescriptorOrError.left().get();
            if (!isBlank(error)) {
                resp.sendError(SC_BAD_REQUEST, error);
            }
            return null;
        }

        return entityDescriptorOrError.right().get().getRepository();
    }
}
